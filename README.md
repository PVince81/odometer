# Odometer

This project is an odometer service for a stationary bicycle written in Python.

## Features

- samples impulses from a Raspberry Pi's GPIO input to calculate the current cycling speed
- statistics are sent via [Socket.io](https://socket.io/) to the [Odometer Server service](https://gitlab.com/PVince81/odometer-server).

## Requirements

### Hardware

- Rasperry Pi (for example the Raspberry Pi Zero with Raspbian 9)
- Stationary bicycle with a switch sensor installed on the wheel

### Software

- Python 3.5+
- [Odometer Server service](https://gitlab.com/PVince81/odometer-server), possibly running on the same device

## Installation

```
% git clone https://gitlab.com/PVince81/odometer.git
% sudo apt-get install virtualenv
% virtualenv ~/.virtualenv/odometer
% source ~/.virtualenv/odometer/bin/activate
% pip3 install -r requirements.txt
```

## Configuration

Inside the odometer directory:

```
% cp config.ini.sample config.ini
```

Edit the file accordingly.

By default the service will be listening to impulses on the specified GPIO.

## Running

```
% source ~/.virtualenv/odometer/bin/activate
% ./main.py
```

## Debugging

For debugging, impulses can be simulated using the `debug_tick=keyboard` or `debug_tick=random` options in config.ini instead of the GPIO.

