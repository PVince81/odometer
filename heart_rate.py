# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#

import threading

class HeartRate(object):
    def __init__(self, device_address):
        try:
            from gi.repository import GObject
        except ImportError:
            import gobject as GObject

        GObject.threads_init()

        from hplusservice import HPlusService
        self._service = HPlusService(device_address)

        self._callback = None

        self._thread = threading.Thread(target=self._worker)
        self._lock = threading.Lock()

        self._last_rate = 0

        self._terminated = threading.Event()
        self._loop_started = False

    def _worker(self):
        self._service.connect()
        self._service.set_callback(self.on_notify)
        self._service.set_all_day_hr(True)
        self._service.set_heart_rate_measure(True)
        # in case of early termination
        if not self._terminated.is_set():
            with self._lock:
                self._loop_started = True
            self._service.run()
        self._service.disconnect()

    def start(self):
        self._thread.start()

    def stop(self):
        with self._lock:
            if self._loop_started:
                self._service.set_heart_rate_measure(False)
                self._service.quit()
        self._terminated.set()
        self._thread.join()

    def get_heart_rate(self):
        return self._last_rate

    def on_notify(self, data, packet):
        with self._lock:
            if data['data_type'] == 'current_stats' and self._last_rate != data['heart_rate']:
                self._last_rate = data['heart_rate']
                if self._callback is not None:
                    self._callback(self._last_rate)

    def set_callback(self, callback):
        self._callback = callback

