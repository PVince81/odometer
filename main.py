#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import sys
import configparser
import time
import logging

from odometer import OdometerService
from push_service import PushService

class ConfigError(Exception):
    def __init__(self, message):
        self.message = message

class Application(object):

    CONFIG_COMMON = "Common"

    def __init__(self, config_file):
        self._quit = False
        config = self._load_config(config_file)

        self._last_values = None

        log_level = int(config.get(self.CONFIG_COMMON, 'log_level', fallback=20))

        logging.basicConfig(level=logging.WARNING)
        self._logger = logging.getLogger('application')
        self._logger.setLevel(log_level)
        logging.getLogger('application.push_service').setLevel(log_level)

        self._console = (config.get(self.CONFIG_COMMON, 'console', fallback='False').lower() == 'true')

        debug_tick = config.get(self.CONFIG_COMMON, 'debug_tick', fallback=None)
        if debug_tick == 'keyboard' and not self._console:
            raise ConfigError('Cannot use keyboard mode without console')

        gpio_channel = config.get(self.CONFIG_COMMON, 'tick_gpio_channel', fallback=None)
        if debug_tick is None and gpio_channel is None:
            raise ConfigError('gpio_channel required for normal operation')

        self._idle_time = float(config.get(self.CONFIG_COMMON, 'idle_timeout', fallback='10'))

        self._odometer_service = OdometerService(
            wheel_radius_cm=float(config.get(self.CONFIG_COMMON, 'wheel_radius_cm', fallback='6')),
            speed_samples=int(config.get(self.CONFIG_COMMON, 'speed_samples', fallback='4')),
            gpio_channel=int(gpio_channel),
            min_threshold_s=float(config.get(self.CONFIG_COMMON, 'min_threshold_s', fallback='0.2')),
            debug_tick=debug_tick
        )

        self._id = config.get(self.CONFIG_COMMON, 'odometer_id', fallback=None)

        self._heart_rate = 0.0
        self._heart_rate_provider = None

        self._push_service = None
        url = config.get(self.CONFIG_COMMON, 'push_url', fallback=None)
        if url:
            if self._id is None:
                raise ConfigError('Missing odometer_id in config, required for push service')

            self._push_service = PushService(url)

        heart_rate_device = config.get(self.CONFIG_COMMON, 'heart_rate_device', fallback='')
        if heart_rate_device != '':
            from heart_rate import HeartRate
            self._heart_rate_provider = HeartRate(heart_rate_device)
        else:
            self._logger.warning("No heart rate device configured")

    def _load_config(self, config_file):
        parser = configparser.ConfigParser()
        parser.read(config_file)
        try:
            return parser
        except KeyError:
            raise ConfigError('Missing or invalid configuration file. Please check config.ini.sample and rename to config.ini')

    def _print_stats(self, values):
        elapsed = values['elapsed']
        values = (
            (elapsed / 60),
            (elapsed % 60),
            values['rotations'],
            values['distance'],
            values['speedKm'],
            values['speedRpm'],
            values['heartRate'],
            values['lastTick'],
        )
        print("time: %02d:%02d rotations: %i distance: %2.2fm speed: %2.2f km/h %2.2f RPM HR: %i -- last_tick=%f" % values, end="\r", flush=True)
        sys.stdout.flush()

    def _reset(self):
        self._logger.debug('Resetting values')
        self._odometer_service.reset()
        self._heart_rate = 0.0
        self._last_values = None

    def _push_values(self, new_values):
        # only send if values changed
        if self._last_values is not None and self._last_values['rotations'] == new_values['rotations']:
            # if no rotation since, end session

            if self._idle_time > 0 and new_values['elapsed'] - self._last_values['elapsed'] >= self._idle_time:
                # session ended at last tick we received
                self._logger.debug('Idle time reached')
                if self._push_service is not None:
                    self._logger.debug('Ending session')
                    self._push_service.session_end(self._id, self._last_values['lastTick'])
                self._reset()

            return

        if self._last_values is None and self._push_service is not None:
            # starting session
            self._logger.debug('Starting session')
            self._push_service.session_start(self._id, new_values['lastTick'])

        # TODO: if the service lagged, send more than one measurement
        new_values['device_guid'] = self._id
        self._last_values = new_values

        if self._push_service is not None:
            self._push_service.send_values(new_values)

    def quit(self):
        self._logger.info('Telling Odometer to quit')
        self._quit = True

    def get_values(self):
        values = self._odometer_service.get_values()
        heart_rate = 0
        if self._heart_rate_provider is not None:
            heart_rate = self._heart_rate_provider.get_heart_rate()

        values['heartRate'] = heart_rate

        return values

    def _start(self):
        self._odometer_service.start()
        if self._heart_rate_provider is not None:
            self._heart_rate_provider.start()

    def _stop(self):
        self._odometer_service.stop()

        if self._heart_rate_provider is not None:
            self._heart_rate_provider.stop()

        if self._push_service is not None:
            self._push_service.close()

    def run(self):
        values = None
        try:

            if self._console:
                print("-------- Odometer --------")
                print();

                print("Hello, please start cycling...")
                print()

            self._start()
            self._logger.info('Odometer started')

            while not self._quit:
                time.sleep(1)

                values = self.get_values()
                if values['elapsed'] is None:
                    # not started yet
                    continue

                if self._heart_rate_provider is not None:
                    self._heart_rate = self._heart_rate_provider.get_heart_rate()

                if self._console:
                    self._print_stats(values)

                self._push_values(values)

        except KeyboardInterrupt:
            pass
        finally:
            self._logger.info('Odometer ending')

            if self._push_service is not None and values is not None and values['lastTick'] is not None:
                self._logger.debug('Ending session')
                self._push_service.session_end(self._id, values['lastTick'])

            self._stop()

        return 0

if __name__ == '__main__':
    # workaround for heart_rate glib loop

    try:
        application = Application('config.ini')
    except ConfigError as config_error:
        print('Configuration error: ', config_error.message, file=sys.stderr)
        sys.exit(1)

    import signal
    signal.signal(signal.SIGINT, lambda *args: application.quit())
    sys.exit(application.run())

