# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#

import logging
import json

class PushService(object):
    def __init__(self, target_url):
        from socketIO_client import SocketIO, LoggingNamespace

        self._logger = logging.getLogger('application.push_service')
        self._logger.debug('Connecting to server')
        self._connection = SocketIO(target_url)

        logging.getLogger('requests').setLevel(logging.WARNING)

    def close(self):
        self._logger.debug('Disconnecting from server')
        self._connection.disconnect()

    def send_values(self, values):
        self._logger.debug('Sending values to server: ' + json.dumps(values))
        self._connection.emit('odometer_values', values)

    def session_start(self, device_guid, start_time):
        self._logger.debug('Starting session for %s at %f' % (device_guid, start_time))
        self._connection.emit('odometer_session_start', {'device_guid': device_guid, 'start_time': start_time})

    def session_end(self, device_guid, end_time):
        self._logger.debug('Ending session for %s at %f' % (device_guid, end_time))
        self._connection.emit('odometer_session_end', {'device_guid': device_guid, 'end_time': end_time})

