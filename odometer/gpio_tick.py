# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import RPi.GPIO as GPIO
import time

from .tick_provider import TickProvider

class GpioTick(TickProvider):
    """GPIO generated tick"""

    def __init__(self, channel):
        super().__init__()
        self._channel = channel
        self._last_tick = None
        self._started = False

    def start(self):
        """Start tick provider and initialize GPIO"""
        if self._started:
            return

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._channel, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(self._channel, GPIO.FALLING, callback=self._gpio_callback, bouncetime=200)
        self._started = True

    def stop(self):
        """Stop tick provider and cleanup GPIO"""
        if self._started:
            GPIO.cleanup()
        self._started = False

    def _gpio_callback(self, channel):
        self._last_tick = time.time()

        # TODO: mutex?
        if self._callback is not None:
            self._callback()

    def wait(self):
        """Wait forever for GPIO ticks"""
        last = self._last_tick
        while last == self._last_tick and self._started:
            time.sleep(0.5)

