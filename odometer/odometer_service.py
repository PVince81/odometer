# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import threading
import logging
import time

from .odometer import Odometer
from .debounce_tick import DebounceTick

class OdometerService(object):

    def __init__(
            self,
            wheel_radius_cm=5,
            speed_samples=3,
            gpio_channel=None,
            min_threshold_s=0.2,
            debug_tick=None
        ):
        self._odometer = Odometer(
            wheel_radius_cm=wheel_radius_cm,
            speed_samples=speed_samples
        )

        self._tick_provider = None

        if debug_tick is None:
            if gpio_channel is not None:
                try:
                    from .gpio_tick import GpioTick
                    self._tick_provider = GpioTick(gpio_channel)
                except ImportError:
                    logging.warning("No GPIO library found, falling back to random tick mode")
                    self._tick_provider = None

        if debug_tick == 'keyboard':
            logging.info('Keyboard tick mode')
            from .keyboard_tick import KeyboardTick
            self._tick_provider = KeyboardTick()

        if debug_tick == 'random' or self._tick_provider is None:
            logging.info('Random tick mode')
            from .random_tick import RandomTick
            self._tick_provider = RandomTick()

        self._tick_provider = DebounceTick(self._tick_provider, min_threshold_s)
        self._tick_provider.set_callback(self._on_tick)

        self._thread = threading.Thread(target=self._worker)
        self._lock = threading.Lock()

        self._terminated = threading.Event()

    def _worker(self):
        # FIXME: will not terminate if stuck in wait
        self._tick_provider.start()
        while not self._terminated.is_set():
            self._tick_provider.wait()
        self._tick_provider.stop()

    def start(self):
        self._thread.start()

    def stop(self):
        self._terminated.set()

    def reset(self):
        with self._lock:
            self._odometer.reset()

    def get_values(self):
        with self._lock:
            values = {
                'elapsed': self._odometer.get_elapsed(),
                'rotations': self._odometer.get_rotations(),
                'distance': self._odometer.get_distance(),
                'speedKm': self._odometer.get_speed(),
                'speedRpm': self._odometer.get_speed_rpm(),
                'lastTick': self._odometer.get_last_tick()
            }
        return values

    def _on_tick(self):
        timestamp = time.time()
        with self._lock:
            self._odometer.tick(timestamp)

