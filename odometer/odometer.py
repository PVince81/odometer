# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import sys

import collections
import time
import math

class Odometer(object):

    def __init__(self, wheel_radius_cm = 5, speed_samples = 3):
        self._circ = wheel_radius_cm / 100 * math.pi * 2
        self._speed_samples = speed_samples

        # times of a full rotation, 0 is the starting timestamp
        self._rotation_times = collections.deque(maxlen=self._speed_samples)

        self.reset()

    def reset(self):
        self._rotation_times.clear()

        # number of rotations
        self._rotations = 0
        # distance in meters
        self._distance = 0.0

        self._start_time = None
        self._last_tick = None

        # speed in km/h
        self._speed = 0.0

        # speed in rotations per minute
        self._speed_rpm = 0.0

    def tick(self, timestamp = None):
        if timestamp is None:
            timestamp = time.time()

        # start counting on first tick
        if self._start_time is None:
            self._start_time = timestamp
        else:
            self._rotations += 1

        self._last_tick = timestamp

        self._rotation_times.append(timestamp)
        self._distance += self._circ

        self._update_speed()

    def _update_speed(self):
        if len(self._rotation_times) > 2:
            # number of seconds between SPEED_SAMPLES rotations (3 samples is 2 rotations)
            diff_time = (self._rotation_times[-1] - self._rotation_times[0])
            #  convert from meters per second to kilometers per hour
            self._speed = self._circ * (len(self._rotation_times) - 0) / diff_time * 18.0 / 5.0

            self._speed_rpm = (len(self._rotation_times) - 1) / (diff_time / 60.0) 

    def get_speed(self):
        return self._speed

    def get_speed_rpm(self):
        return self._speed_rpm

    def get_distance(self):
        return self._distance

    def get_rotations(self):
        return self._rotations

    def get_last_tick(self):
        return self._last_tick

    def get_elapsed(self):
        if self._start_time is not None:
            return time.time() - self._start_time
        return None

