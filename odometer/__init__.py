# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#

from .odometer_service import OdometerService
from .tick_provider import TickProvider

