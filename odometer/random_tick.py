# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import time
import random
import logging

from .tick_provider import TickProvider

class RandomTick(TickProvider):
    """Generate ticks randomly"""

    def __init__(self, pause_duration=20, pause_chance_percent=1.0):
        super().__init__()
        self._pause_duration = pause_duration
        self._pause_chance_percent = pause_chance_percent
        self._paused = False

    def wait(self):
        """Sleep for random amounts of time before generating a tick"""
        # regular tick
        if self._paused:
            time.sleep(self._pause_duration)
            logging.debug("End of pause")
            self._paused = False
        elif random.random() * 100.0 <= self._pause_chance_percent:
            logging.debug("Starting pause")
            self._paused = True

        jitter = random.random() * 0.2 - 0.1
        wait_time = 1.0 + jitter
        logging.debug("Waited %.2f" % wait_time)
        time.sleep(wait_time)

        if self._callback is not None:
            self._callback()


