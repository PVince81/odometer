# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
class TickProvider(object):
    """Base class for tick providers"""

    def __init__(self):
        self._callback = None

    def start(self):
        """Start tick provider"""
        pass

    def stop(self):
        """Stop tick provider"""
        pass

    def set_callback(self, callback):
        """Set tick callback which gets call on every tick

        :param callback: tick callback
        """
        self._callback = callback

    def wait(self):
        """Wait for tick to occur"""
        pass

