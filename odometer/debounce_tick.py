# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
import time
from .tick_provider import TickProvider

class DebounceTick(TickProvider):
    """Decorator tick provider to debounce ticks"""

    def __init__(self, tick_provider, min_threshold_s=0.2):
        super().__init__()
        self._tick_provider = tick_provider
        self._tick_provider.set_callback(self._debounce_callback)

        self._last_tick = None
        self._last_diff = 0.0

        self._min_threshold_s = min_threshold_s

    def start(self):
        self._tick_provider.start()

    def stop(self):
        self._tick_provider.stop()

    def set_callback(self, callback):
        self._callback = callback

    def _debounce_callback(self):
        diff = 0.0

        now = time.time()

        # at least two ticks
        if self._last_tick is not None:
            diff = now - self._last_tick

        if diff > 0.0 and diff < self._min_threshold_s:
            # skip tick if tick happened too fast
            return

        self._last_diff = diff
        self._last_tick = now

        if self._callback is not None:
            self._callback()

    def wait(self):
        self._tick_provider.wait()

