# -*- coding: utf-8 -*-
#
# vim: expandtab shiftwidth=4 softtabstop=4
#
from .tick_provider import TickProvider

class KeyboardTick(TickProvider):
    """Provide ticks when user presses enter"""

    def start(self):
        """Start tick provider and print a message"""
        print("Keyboard mode: hit enter to simulate a rotation")

    def wait(self):
        """Wait for keyboard input"""
        input()
        if self._callback is not None:
            self._callback()


